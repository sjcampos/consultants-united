CREATE DATABASE 'name';

use 'name';

-- You have to run every script in order from top to bottom -----

CREATE TABLE Agency(
	AgencyId int IDENTITY(1,1) PRIMARY KEY,
	AgencyCode int NOT NULL,
	AgencyName varchar(255),
	AgencyPro  varchar(255),
	AgencyCant varchar(255),
	AgencyDistr varchar(255)
);

CREATE TABLE Departments(
	DepartmentId int IDENTITY(1,1) PRIMARY KEY,
	Department_name varchar(255) NOT NULL,
	Agency_Id int NOT NULL FOREIGN KEY REFERENCES Agency(AgencyId)
);

CREATE TABLE Employee (
    EmployeeId int IDENTITY(1,1) PRIMARY KEY,
	PersonalId varchar(255) NOT NULL,
    FirstName varchar(255) NOT NULL,
    Lastname varchar(255),
    Email varchar(255),
	Department_code int NOT NULL FOREIGN KEY REFERENCES Departments(DepartmentId)
);

CREATE TABLE Suppliers(
	SupplierId int IDENTITY(1,1) PRIMARY KEY,
	CompanyName varchar(255),
	Telephone varchar(255),
	Email varchar(255),
	SupplierPro  varchar(255),
	SupplierCant varchar(255),
	SupplierDistr varchar(255)
);


CREATE TABLE Products(
	ProductId int IDENTITY(1,1) PRIMARY KEY,
	SupplierId int NOT NULL FOREIGN KEY REFERENCES Suppliers(SupplierId),
	Description varchar(255) NOT NULL,
	UnitsInStock int NOT NULL,
	Price varchar(255) NOT NULL,
	Discontinued bit DEFAULT 0
);

CREATE TABLE Clients(
	ClientId  int IDENTITY(1,1) PRIMARY KEY,
	PersonalId varchar(255) NOT NULL,
	Name varchar(255) NOT NULL,
	Age int NOT NULL,
	Telephone varchar(255) NOT NULL,
	Email varchar(255)

);

CREATE TABLE Sales(
	SaleId int IDENTITY(1,1) PRIMARY KEY,
	EmployeeId int NOT NULL FOREIGN KEY REFERENCES Employee(EmployeeId),
	ClientId int NOT NULL FOREIGN KEY REFERENCES Clients(ClientId),
	Date varchar(255)
);

CREATE TABLE ProductsBill(
	Id int IDENTITY(1,1) PRIMARY KEY,
	SalesId int NOT NULL FOREIGN KEY REFERENCES Sales(SaleId),
	ProductId int NOT NULL FOREIGN KEY REFERENCES Products(ProductId),
	Quantity int
);



