use 'name';

-- Run every script from top to bottom, do not run all the scripts at once, run by groups

---- Agency Inserts ------------ 

insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (01, 'CentralCQ', 'Alajuela', 'San Carlos', 'Ciudad Quesada');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (01, 'CentralAla', 'Alajuela', 'Alajuela', 'Alajuela');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (02, 'CentralHeredia', 'Heredia', 'Flores', 'San Joaquin de Flores');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (02, 'DistBarva', 'Heredia', 'Barva', 'Barva');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (02, 'DistIsidro', 'Heredia', 'San Isidro', 'Concepcion');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (03, 'CentralCartago', 'Cartago', 'Cartago', 'Corralillo');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (03, 'DistParaiso', 'Cartago', 'Paraiso', 'Paraiso');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (04, 'DistPuerto', 'Puntarenas', 'Esparza', 'San Rafael');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (04, 'DistPaquera', 'Puntarenas', 'Puntarenas', 'Paquera');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (05, 'DistLimon', 'Limon', 'Limon', 'Limon');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (05, 'CentralLimon', 'Limon', 'Pococ�', 'Guapiles');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (06, 'CentralLiberia', 'Guanacaste', 'Liberia', 'Liberia');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (06, 'DistCa�as', 'Guanacaste', 'Ca�as', 'Ca�as');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (07, 'CentralSJ', 'San Jos�', 'San Jos�', 'Zapote');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (07, 'DistDesamparados', 'San Jos�', 'Desamparados', 'San Rafael Arriba');
insert into Agency (AgencyCode, AgencyName, AgencyPro, AgencyCant, AgencyDistr) values (07, 'DistEscazu', 'San Jos�', 'Escaz�', 'San Antonio');


SELECT * FROM Agency;
------------------------------------------------------------------------------
----- Department Inserts-----------

insert into Departments (Department_name, Agency_Id) values ('Supermercado', 1);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 1);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 2);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 2);
insert into Departments (Department_name, Agency_Id) values ('Ropa', 2);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 3);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 3);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 4);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 4);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 5);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 5);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 6);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 6);
insert into Departments (Department_name, Agency_Id) values ('Supermercado', 7);
insert into Departments (Department_name, Agency_Id) values ('Electronicos', 7);
insert into Departments (Department_name, Agency_Id) values ('Ropa', 7);

SELECT * FROM Departments;

-------------------------------------------------------------------------------------
----------- Employee Inserts ------------------------------------
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('774-01-5783', 'Ellary', 'Guillot', 'eguillot0@linkedin.com', 1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('812-30-5044', 'Kaine', 'Brigham', 'kbrigham1@ihg.com', 1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('735-23-8357', 'Madel', 'Exroll', 'mexroll2@arstechnica.com', 1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('825-38-3207', 'Roxanna', 'Isley', 'risley3@answers.com', 1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('126-05-0399', 'Korey', 'Bowich', 'kbowich4@nhs.uk', 1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('797-42-5919', 'Donica', 'Cuolahan', 'dcuolahan5@vinaora.com',1);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('189-99-4426', 'Cristi', 'Ronchetti', 'cronchetti6@google.com', 2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('524-01-5009', 'Forbes', 'Arnaudot', 'farnaudot7@imageshack.us',2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('313-08-3490', 'Lowe', 'Perocci', 'lperocci8@plala.or.jp', 2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('843-19-5393', 'Erasmus', 'Bursnoll', 'ebursnoll9@seattletimes.com', 2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('302-20-4078', 'Lorne', 'Mungan', 'lmungana@dropbox.com', 2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('892-08-9901', 'Dwain', 'Ivy', 'divyb@nhs.uk', 2);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('520-88-7257', 'Hilda', 'Trowsdale', 'htrowsdalec@weibo.com', 3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('766-99-7185', 'Dody', 'MacDwyer', 'dmacdwyerd@mayoclinic.com', 3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('130-49-1092', 'Woody', 'Olekhov', 'wolekhove@dmoz.org',3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('132-21-8149', 'Rheba', 'Hathaway', 'rhathawayf@webs.com', 3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('368-04-2523', 'Moe', 'Farmer', 'mfarmerg@disqus.com', 3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('535-61-8247', 'Ronica', 'Iveans', 'riveansh@studiopress.com', 3);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('702-94-3328', 'Timmie', 'Le Blond', 'tleblondi@booking.com', 4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('294-05-0707', 'Thedrick', 'De la Barre', 'tdelabarrej@stumbleupon.com', 4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('436-98-4612', 'Corina', 'McGeachey', 'cmcgeacheyk@tiny.cc', 4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('678-23-6226', 'Ogdan', 'Lawler', 'olawlerl@discuz.net', 4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('622-46-7147', 'Magdalene', 'Murthwaite', 'mmurthwaitem@mediafire.com',4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('561-72-7261', 'Carmen', 'Wixey', 'cwixeyn@samsung.com', 4);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('492-24-7908', 'William', 'Berryann', 'wberryanno@umich.edu', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('759-57-9946', 'Geno', 'Treagust', 'gtreagustp@friendfeed.com', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('507-95-2522', 'Gaston', 'Celes', 'gcelesq@pagesperso-orange.fr', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('614-42-5813', 'Clement', 'Toye', 'ctoyer@ucla.edu', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('440-07-2458', 'Boony', 'Leidecker', 'bleideckers@rakuten.co.jp', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('114-35-6507', 'Gabie', 'Cadden', 'gcaddent@sbwire.com', 5);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('475-50-7081', 'Inglis', 'Swinden', 'iswindenu@google.co.jp', 6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('713-86-5365', 'Kaleb', 'Nast', 'knastv@ibm.com', 6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('283-22-4401', 'Teddie', 'Bowerbank', 'tbowerbankw@de.vu',6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('623-17-7106', 'Sauncho', 'Inkin', 'sinkinx@nsw.gov.au', 6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('861-29-1882', 'Iormina', 'Lyall', 'ilyally@wikimedia.org', 6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('853-94-5362', 'Lorrayne', 'Asman', 'lasmanz@ebay.co.uk', 6);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('563-27-1396', 'Andi', 'Donalson', 'adonalson10@istockphoto.com',7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('676-91-8526', 'Samuel', 'Cruft', 'scruft11@dagondesign.com', 7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('131-72-4046', 'Lynsey', 'Spikeings', 'lspikeings12@dion.ne.jp',7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('424-31-4651', 'Brunhilde', 'Gurdon', 'bgurdon13@google.com', 7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('162-88-6006', 'Peri', 'Tofful', 'ptofful14@amazon.co.uk', 7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('589-76-1296', 'Paton', 'Kauschke', 'pkauschke15@bandcamp.com', 7);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('442-66-9437', 'Myer', 'McBay', 'mmcbay16@redcross.org', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('485-77-5404', 'Darrick', 'Samwyse', 'dsamwyse17@ca.gov', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('618-63-2418', 'Aldric', 'Agett', 'aagett18@soundcloud.com', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('812-28-0456', 'Tory', 'MacSwayde', 'tmacswayde19@nba.com', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('311-27-9707', 'Burton', 'Carmo', 'bcarmo1a@timesonline.co.uk', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('664-94-7012', 'Winthrop', 'Pentercost', 'wpentercost1b@mail.ru', 8);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('300-44-4269', 'Horace', 'Bartlet', 'hbartlet1c@taobao.com', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('468-87-3191', 'Isahella', 'Aronow', 'iaronow1d@cpanel.net', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('717-61-2074', 'Elias', 'Challin', 'echallin1e@shinystat.com', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('883-49-7863', 'Dwain', 'Baynham', 'dbaynham1f@theatlantic.com', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('399-32-4013', 'Cyrille', 'Braghini', 'cbraghini1g@360.cn', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('222-48-8202', 'Emilie', 'Waldrum', 'ewaldrum1h@thetimes.co.uk', 9);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('457-73-2258', 'Shauna', 'Gerrie', 'sgerrie1i@issuu.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('750-72-3017', 'Lindsey', 'Gorring', 'lgorring1j@arstechnica.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('678-33-2906', 'Dicky', 'Owain', 'dowain1k@yahoo.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('856-58-8145', 'Evin', 'Cresar', 'ecresar1l@merriam-webster.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('444-36-6942', 'Carmella', 'Pourvoieur', 'cpourvoieur1m@cdbaby.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('423-22-2300', 'Valle', 'Zupo', 'vzupo1n@quantcast.com', 10);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('230-68-9144', 'Roxane', 'Litterick', 'rlitterick1o@issuu.com', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('213-67-4433', 'Dulce', 'Pettipher', 'dpettipher1p@senate.gov', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('596-34-4972', 'Zerk', 'Vernazza', 'zvernazza1q@pinterest.com', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('454-66-6031', 'Flore', 'Maclean', 'fmaclean1r@slashdot.org', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('757-77-6137', 'Isabeau', 'Bonhill', 'ibonhill1s@google.com.hk', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('100-46-5181', 'Frazier', 'Blazey', 'fblazey1t@newsvine.com', 11);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('481-23-3861', 'Nerta', 'Rosi', 'nrosi1u@unblog.fr', 12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('519-41-5942', 'Baxy', 'Twine', 'btwine1v@blogger.com', 12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('854-99-7568', 'Valera', 'Stonhewer', 'vstonhewer1w@lulu.com',12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('491-04-9735', 'Joshua', 'Quare', 'jquare1x@last.fm', 12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('444-19-9200', 'Marjorie', 'Kulas', 'mkulas1y@godaddy.com',12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('400-65-8321', 'Chandler', 'Fuzzard', 'cfuzzard1z@cisco.com', 12);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('157-06-3413', 'Adolf', 'Downie', 'adownie20@google.cn', 13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('316-57-5497', 'Aurore', 'Atteridge', 'aatteridge21@ftc.gov',13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('693-98-9498', 'Eustace', 'Rivel', 'erivel22@hc360.com', 13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('837-82-1629', 'Harmonia', 'Tammadge', 'htammadge23@hibu.com', 13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('857-62-1203', 'Modesta', 'Attree', 'mattree24@yellowpages.com', 13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('557-21-5521', 'Alie', 'Bierton', 'abierton2n@mtv.com', 13);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('275-22-7949', 'Lon', 'Boycott', 'lboycott25@a8.net', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('528-15-2030', 'Rance', 'Smurfitt', 'rsmurfitt26@shareasale.com', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('276-09-7974', 'Giana', 'Clara', 'gclara27@hostgator.com', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('473-14-0145', 'Bartie', 'Freake', 'bfreake28@live.com', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('202-98-6298', 'Ferdy', 'Isaaksohn', 'fisaaksohn29@facebook.com', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('529-56-1740', 'Aldous', 'Mongan', 'amongan2a@qq.com', 14);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('544-18-5510', 'Tito', 'Baum', 'tbaum2b@adobe.com', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('641-92-2995', 'Karyl', 'Romushkin', 'kromushkin2c@reverbnation.com', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('408-12-8346', 'Dominique', 'Inggall', 'dinggall2d@themeforest.net', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('742-64-6400', 'Melisande', 'Feldhuhn', 'mfeldhuhn2e@economist.com', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('230-35-5598', 'Oralle', 'Stocking', 'ostocking2f@hibu.com', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('705-72-5418', 'Theresa', 'Mathers', 'tmathers2g@ucoz.com', 15);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('319-12-6728', 'Yalonda', 'Bushby', 'ybushby2h@amazonaws.com', 16);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('686-70-2486', 'Salvidor', 'Liveley', 'sliveley2i@va.gov',16);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('376-86-1595', 'Farrand', 'Creagh', 'fcreagh2j@jiathis.com', 16);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('735-46-3927', 'Lise', 'Salvador', 'lsalvador2k@dropbox.com', 16);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('533-24-6499', 'Kaia', 'Gedge', 'kgedge2l@shareasale.com', 16);
insert into Employee (PersonalId, FirstName, Lastname, Email,  Department_code) values ('307-59-0900', 'Teressa', 'Trebble', 'ttrebble2m@kickstarter.com', 16);

SELECT * FROM Employee;
-----------------------------------------------------------------------------------------------------------------------------------------------

----------------Suppliers Inserts------------------------------------------

insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Brightdog', '168-21-5617', 'kcrone0@addtoany.com', 'Heredia', 'Barva', 'Barva');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Buzzdog', '680-27-9352', 'dmarval1@japanpost.jp', 'San Jos�','Goicoechea','Guadalupe');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Quinu', '547-08-0293', 'ecamamill2@reverbnation.com', 'Alajuela', 'San Ramon', 'San Ramon');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Voonyx', '482-53-3084', 'tleigh3@wikimedia.org', 'Heredia', 'Heredia', 'San Francisco');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Dabtype', '726-64-8401', 'tantram4@omniture.com', 'San Jos�', 'Desamparados', 'San Cristobal');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Eadel', '363-55-6160', 'jchanders5@noaa.gov', 'Heredia', 'Santo Domingo', 'Santo Domingo');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Quimba', '265-45-2703', 'painsby6@unblog.fr', 'Alajuela', 'Grecia', 'Grecia');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Skipstorm', '320-43-9221', 'esuccamore7@amazon.co.jp', 'Cartago', 'Para�so', 'Para�so');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Oloo', '899-15-1795', 'anerger8@answers.com', 'San Jos�', 'Alajuelita', 'San Antonio');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Devpoint', '257-64-9464', 'bsauniere9@studiopress.com', 'Alajuela', 'Alajuela', 'Alajuela');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Blogtags', '812-70-2390', 'vrehora@newsvine.com', 'Cartago', 'Cartago', 'Llano Grande');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Muxo', '191-06-2305', 'vdonneb@parallels.com', 'Puntarenas', 'Esparza', 'San Juan Grande');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Rhybox', '111-40-2696', 'kcaseroc@mit.edu', 'San Jos�', 'San Jos�', 'Zapote');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Camimbo', '380-90-1001', 'hsellyd@mediafire.com', 'Heredia', 'Flores', 'San Joaquin de Flores');
insert into Suppliers (CompanyName, Telephone, Email, SupplierPro, SupplierCant, SupplierDistr) values ('Blogspan', '328-01-4583', 'hskallse@weibo.com', 'Alajuela', 'Naranjo', 'Naranjo');

SELECT * FROM Suppliers;
-----------------------------------------------------------------------------------------------------------------------------------
---------Products Inserts-----------------------------------------

insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Bulto de arroz blanco 12 u', 169, '$11.79', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Bulto de frijoles negros 24 u', 1827, '$91.69', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Bulto de frijoles rojos', 469, '$32.82', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Bulto Sal 24 unidades', 1003, '$58.28', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (4, 'Caja 24 unidades mantequilla clarificada', 212, '$58.14', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Caja 6 unidades Galon Aceite', 632, '$89.81', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Bulto az�car 12 u 2kg', 414, '$20.37', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Bulto az�car 12 u 5kg', 44, '$67.68', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Paquete sobres az�car 300 u', 199, '$77.79', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (6, 'Caja sobres tomate 400 u', 1669, '$51.12', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Caja sobres mayonesa 400 u', 459, '$43.72', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Paquete salchichas pavo 30 u', 627, '$85.43', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Paquete salchichas 30 u', 411, '$30.22', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Paquete jam�n pavo 500 g', 1889, '$83.59', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Paquete jam�n corriente 500 g', 1984, '$56.18', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (5, 'Caja tazas plasticas sopera 400 u', 941, '$84.02', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Caja platos plasticos 200 u', 1900, '$54.12', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (14, 'Paquete tenedores plasticos 100 u', 699, '$20.63', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Paquete cucharas plasticas 30 u', 1952, '$10.61', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Caja tortas de carne congeladas 100 u', 1868, '$71.89', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Caja tortas de pollo congeladas 100 u', 259, '$62.67', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (5, 'Bolsa camarones 7 kg', 1027, '$47.12', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (12, 'Bolsa camarones jumbo 6 kg', 1853, '$82.35', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Caja latas 4 litros salsa queso', 950, '$75.18', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (5, 'Caja pulpas para fresco mixta 10 galones', 293, '$41.52', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Pack refrescos 100 u 600ml', 1758, '$82.29', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Papas tostadas mostaza 7 kg', 747, '$33.50', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Servilletas grandes 1000 u', 640, '$27.58', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Paquete cajas plasticas 300 u', 1021, '$30.10', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Paquete queso americano laminas 600 u', 472, '$78.21', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Caja cervezas 100 u', 1306, '$44.99', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Caja lasa�a 20 u', 551, '$48.02', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Paquete crema para caf� 5 kg', 1060, '$31.80', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Caja guantes latex 5000 u', 654, '$30.52', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (12, 'Caja salm�n congelado 10 kg', 602, '$98.62', 0);
--------------------------------------------
SELECT * FROM Products;
--------------------------------------------
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'SSD 540 G', 1115, '$69.35', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Disco duro mecanico 500 G', 1225, '$33.06', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Hub 7 puertos 3.0', 1255, '$25.16', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Portatil Lenovo economica', 1026, '$550.89', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Computadora escritorio PV', 1164, '$250.42', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Computadora gaming standar', 1813, '$850.80', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Cable displayport', 453, '$41.11', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Cable red rj45 Cat6 20 m', 499, '$58.98', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (4, 'SSD 960 G', 1300, '$86.97', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Disco duro mecanico 960 G', 1604, '$58.51', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (5, 'Audifonos razer blackhammer', 1351, '$73.21', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Headset astro a20', 718, '$65.81', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Headset razer Kraken', 503, '$79.48', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Teclado mecanico corsair k95', 284, '$62.99', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Teclado mecanico razer huntsman mini', 1340, '$88.19', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Teclado mecanico Logitech 78P', 1105, '$66.95', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Headset Astro a10', 1489, '$29.85', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Teclado corsair vengance', 1380, '$12.44', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (4, 'Mouse logitech g903', 1391, '$40.44', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Mouse corsair gr Pro', 1868, '$47.61', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Cable HDMI 2 m', 1876, '$10.59', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Headset razer Blackwidow', 1945, '$82.28', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Memoria USB 500 G', 668, '$11.45', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Monitor LG 2k 27 pulgadas', 1108, '$500.95', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Monitor Samsung 60 hz', 1792, '$98.79', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (12, 'Monitor hp 144hz', 1234, '$300.44', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Teclado membrana logitech', 1369, '$62.92', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Cable extensor USB M-H 5 m', 949, '$25.40', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Disco duro externo SSD 540 G', 1039, '$97.76', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (6, 'Disco duro externo  240', 1424, '$30.39', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Portatil HP generica', 235, '$260.95', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (12, 'Tarjeta grafica 1660 Gigabyte', 360, '$276.99', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Impresora HP', 749, '$54.48', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Set keycaps pbt', 587, '$42.06', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Mouse Razer viper ultimate', 42, '$91.44', 0);
-------------------------------------
SELECT * FROM Products;
-------------------------------------
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (14, 'Camiseta Columbia XL', 1003, '$88.15', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Camiseta Columbia L', 1117, '$64.56', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Camiseta Columbia M', 1289, '$82.10', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (6, 'Camiseta nike basket edition L', 150, '$61.11', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (4, 'Camiseta nike basket edition XL', 1393, '$59.13', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Camiseta nike basket editon XXL', 665, '$53.85', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Pantalon Levis 201 38 38', 1602, '$11.46', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Pantalon Levis 201 42 38', 1605, '$23.53', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Camiseta Nike Polo XL', 1316, '$57.13', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Camiseta Nike Polo S', 83, '$61.96', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (8, 'Camiseta Nike Polo L', 1478, '$88.31', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (12, 'Camiseta Nike Polo M', 1179, '$63.27', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Camiseta Nike tirantes XXL', 962, '$22.13', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Camiseta Nike tirantes XL', 1821, '$43.62', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Pantaloneta Champions XL', 886, '$40.53', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Pantaloneta Champions S', 124, '$46.15', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Pantaloneta Champions L', 1313, '$18.84', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Pantalon patito 36', 1782, '$70.98', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (15, 'Medias Addidas blancas', 817, '$49.73', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (13, 'Medias Addidas negras', 268, '$15.05', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Medias Nike verdes para correr', 207, '$28.15', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Medias Addidas para correr', 261, '$73.29', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (11, 'Pantaloneta basketball XL', 1573, '$28.08', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (3, 'Camiseta Under XL', 1022, '$21.23', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (2, 'Pantalon Under 40', 1705, '$26.02', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (1, 'Pantaloneta Azul ejercicio M', 740, '$56.48', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (7, 'Short L', 66, '$76.52', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (9, 'Blusa Holiester s', 1211, '$66.08', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (14, 'Blusa Nike L', 1771, '$44.29', 0);
insert into Products (SupplierId, Description, UnitsInStock, Price, Discontinued) values (10, 'Enagua', 520, '$42.81', 0);
---------------------------------
SELECT * FROM Products;
----------------------------------------------------------------------------------------------------------------------------------------
