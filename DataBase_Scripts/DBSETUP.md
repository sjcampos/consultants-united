# DataBase set up

In the following scripts you will find everything you need to use a database with more than 25 thousand records of data in sales, remember that we must use sql server

## Setting up the tables

Let's set up the database using the following steps

* You need to run this [file](Create_DB.sql) to create the database and the tables, run all the scripts in order from top to bottom.

Now let's insert some basic information before adding all the registers

* Run this [file](BasicData_Inserts.sql) to insert some basic information, run all the scripts in order from top to bottom and do not run all at once, go by groups.

Last but not least

* Run this [file](Clients_Inserts.sql) to insert the last basic information before adding all the registers

## Adding all the transactions

Once we have completed the configuration process successfully, we proceed to run the necessary scripts to fill our database with transactions

**IMPORTANT**

**To successfully complete the inserts of the transactions, it is important to run each file as indicated in the list that follows, because the id's are already linked with all the previous information and the new information that is being inserted.**

**RUN EVERY FILE IN ORDER from 2015 to 2020**

* First file [2015 sales](Sales_Scripts/Sales_2015.sql)
* Second file [2016 sales](Sales_Scripts/Sales_2016.sql)
* Third file [2017 sales](Sales_Scripts/Sales_2017.sql)
* Fourth file [2018 sales](Sales_Scripts/Sales_2018.sql)
* Fifth file [2019 sales](Sales_Scripts/Sales_2019.sql)
* Sixth file [2020 sales](Sales_Scripts/Sales_2020.sql)

Continue with the next files

**RUN EVERY FILE IN ORDER from 2015 to 2020**

* First file [2015 bills](ProductsBill_Scripts/ProductsBill_2015.sql)
* Second file [2016 bills](ProductsBill_Scripts/ProductsBill_2016.sql)
* Third file [2017 bills](ProductsBill_Scripts/ProductsBill_2017.sql)
* Fourth file [2018 bills](ProductsBill_Scripts/ProductsBill_2018.sql)
* Fifth file [2019 bills](ProductsBill_Scripts/ProductsBill_2019.sql)
* Sixth file [2020 bills](ProductsBill_Scripts/ProductsBill_2020.sql)


If each of the steps was successfully completed, then the other steps of the project can be continued. 

Click [here](../README.md) to go back.