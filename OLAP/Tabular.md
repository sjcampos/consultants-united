# OLAP 

# Tabular

    The Tabular model "in memory"  gives better performance than the multidimensional model. It also makes direct use of client reports with Excel and PowerPivot, this reduces the infrastructure and coding to generate analytical reports. In addition, Tabular offers another model, DirectQuery, which allows direct queries against the relational data source, being very appropriate for large volumes of data.
    
## Requirements

[SQL Server Data Tools](https://docs.microsoft.com/en-us/sql/ssdt/download-sql-server-data-tools-ssdt?view=sql-server-ver15)

## Steps

### Adding our Data Source

    First we are going to create a new tabular project.

![alt text](../Images/NewConnectionTabular.png "NewProject")


    Then we are going to create a new connection.

![alt text](../Images/AddingDataSource.png "Tabular")

    Now we can select our data source

![alt text](../Images/TabularConnection.png "DataSource")

    With our data source added, we can select our tables.

![alt text](../Images/SelectTables.png "Tables")


    Now that we have our data source and we can use the information, we can play with it.

![alt text](../Images/TabularDato.png "TabularDato")

Click [here](../README.md) to go back.