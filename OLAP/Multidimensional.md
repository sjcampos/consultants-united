# OLAP 

# MULTIDIMENSIONAL

    OLAP cubes are multidimensional structures (cubes) that allow you to analyze relational databases of great volume and variety with great agility and speed, greatly reducing the time and resources used in analysis.

## Requirements

[SQL Server Data Tools](https://docs.microsoft.com/en-us/sql/ssdt/download-sql-server-data-tools-ssdt?view=sql-server-ver15)

## Steps

### Adding our Data Source

    First we are going to create  a new project in Visual Studio 2019, specific a Analysis Services project.

![alt text](../Images/new_project.png "NewProject")

    Then we are going add a new data source.

![alt text](../Images/selectDB.png "DB")

    so we need to create a new connection.

![alt text](../Images/new_connection.png "NewConnection")

    Now with the connection created, we need to verify if the credentials are added.

![alt text](../Images/credentialsVerification.png "Credentials")

### New Data Source View

    Now that we have a data source we can create a data source view.


![alt text](../Images/DataView.png "DataView")

    Then we are going to select our fact table.

![alt text](../Images/AddFacts.png "FactTable")

    With SQL Server Data Tools, we just have to select our fact table, if everything is good the program will select all dimensions.

    Now we can see our scheme by double clicking above our new data source view.

### New Cube

    We have our data source view, now we can create a new cube.

![alt text](../Images/NewCube.png "NewCube")

    Then we need to select a fact table.

![alt text](../Images/SelectFactTable.png "FactTable")

    Now we can continue, because the fact table selected all dimensions.

    We can name our cube.

![alt text](../Images/CubeName.png "CubeName")

    Finally we can see our data flow and scheme.

![alt text](../Images/complete.png "Complete")

    Now we can chose what kind of elements with need in our dimensions to complete our cube.

![alt text](../Images/DimensionEdit.png "DimensionEdit")

    When we finish to chose our elements we can process our cube.

![alt text](../Images/pros.png "Pros")


    With that we complete our cube and we can explore the data.

![alt text](../Images/brow.png "Brow")

Click [here](../README.md) to go back.