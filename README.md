# Data mining project

This the first project of the data mining class

## Documento enunciado 1

Click [here](Documento_Enunciado_1/Proyecto_1.pdf )

## Requirements

### [SQL Server](https://www.microsoft.com/es-es/sql-server/sql-server-downloads) and [Managment Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)

First you will need a database, we are using our own and [here](DataBase_Scripts/DBSETUP.md) you can learn how to set up the database

## Dimensions and Fact Tables

For setting up the dimensions tables click [here](Dimensions_Scripts/Dimensions.md)



## OLAP

For the OLAP proccess we have two different types

[Multidimensional](OLAP/Multidimensional.md) 

[Tabular](OLAP/Tabular.md) 



## BACKUPS

Here are the backups for the differents databases that we generate during the project

[Staging Area](BACKUP/TestProSA.bak)

[Datawarehouse](BACKUP/TestProDW.bak)