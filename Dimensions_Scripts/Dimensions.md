## How to set up the dimensions DB

First you need to create a new database where you can run all the dimensions scripts.

After that you just need to run the follow scripts in order:

[Agency Dim](AgencyDim_Script.sql)

[Client Dim](ClientDim_Script.sql)

[Department Dim](DepartmentDim_Script.sql)

[Employee Dim](EmployeeDim_Script.sql)

[ProductBill Dim](ProductBillDim_Script.sql)

[Product Dim](ProductDim_Script.sql)

[Provider Dim](ProviderDim_Script.sql)

[Sale Dim](SaleDim_Script.sql)

[Time Dim](TimeDim_Script.sql)

Once all the dim scripts have been run, all you have to do is run the following script to create the fact table -> [Fact table](../Facts_Scripts/FactSales_Script.sql)

After this you should have a diagram similar to this image

![alt text](../Images/Diagram.png "Diagram")

And thats how you set up the dimensions tables

Click [here](../README.md) to go back