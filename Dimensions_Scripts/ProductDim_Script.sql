--Productos --
CREATE TABLE DIM_PRODUCT(
ID_PRODUCT INT IDENTITY CONSTRAINT PK_PRODUCT PRIMARY KEY,
SUPPLIER_COD NVARCHAR(255),
DESCRIPTION_PRODUCT NVARCHAR(255),
UNITS_IN_STOCK NVARCHAR(255),
PRICE NVARCHAR(255),
DICONTINUED NVARCHAR(255)
)