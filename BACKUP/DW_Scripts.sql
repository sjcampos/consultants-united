use TestProSA;
---DIM AGENCY-----
select 
AgencyCode identification, 
AgencyName name_agnecy,
AgencyPro province_name,
AgencyCant canton_name,
AgencyDistr district_name
from TestProSA.dbo.Agency;
-----------------------------

------DIM CLIENT-----------
select 
PersonalId IDENTIFICATION,
Name FULL_NAME,
Age ACTUAL_AGE,
Telephone PHONE_NUMBER,
Email EMAIL
from TestProSA.dbo.Clients;
-----------------------------

-----DIM DEPARTMENT-----------
select 
Department_name FULL_NAME,
Agency_Id AGENCY_CODE
from TestProSA.dbo.Departments;
-----------------------------------

--------DIM EMPLOYEE -----------------------
select 
PersonalId IDENTIFICATION,
FirstName FIRSTNAME,
Lastname LASTNAME,
Email EMAIL,
Department_code DEPARTMENT_CODE
from TestProSA.dbo.Employee;
---------------------------------

-------DIM PRODUCT-------------
select 
SupplierId SUPPLIER_CODE,
Description DESCRIPTION_PRODUCT,
UnitsInStock UNITS_IN_STOCK,
Price PRICE,
iif(p.Discontinued = 0,'NO','SI')DISCONTINUED
from TestProSA.dbo.[ Products] p;
-----------------------------------------

----DIM PRODUCT_BILL---------
select 
pb.SalesId SALES_CODE,
pb.ProductId PRODUCT_CODE,
(CAST(TestProSA.dbo.[ Products].Price as Money) * pb.Quantity)AMOUNT_OF_BILL
from TestProSA.dbo.ProductsBill pb
inner join TestProSA.dbo.[ Products] on TestProSA.dbo.[ Products].ProductId = pb.ProductId;
---------------------------------------------------

-----DIM PROVIDER -----------
select 
CompanyName COMPANY_NAME,
Telephone PHONE_NUMBER,
Email EMAIL,
SupplierPro PROVINCE_NAME,
SupplierCant CANTON_NAME,
SupplierDistr DISTRICT_NAME
from TestProSA.dbo.Suppliers;
--------------------------------------

-----DIM SALE--------------
SET DATEFORMAT dmy;
select 
EmployeeId EMPLOYEE_CODE,
ClientId CLIENT_CODE,
TRY_CAST(s.Date as date)FULL_DATE
from TestProSA.dbo.Sales s;
---------------------------------

-----DIM TIME---------------
SET DATEFORMAT dmy;
select 
TRY_CAST(s.Date as date) DATE_TIME,
YEAR(TRY_CAST(s.Date as date)) YEAR_TIME,
MONTH(TRY_CAST(s.Date as date)) MONTH_TIME,
DAY(TRY_CAST(s.Date as date)) YEAR_DAY,
DATEPART(weekday,TRY_CAST(s.Date as date)) WEEK_DAY
from TestProSA.dbo.Sales s;


---- DIM FACT SALES-------------------------------------------
use TestProSA;

SET DATEFORMAT dmy;
SELECT
c.ID_CLIENT ID_CLIENT,
e.ID_EMPLOYEE ID_EMPLOYEE,
d.ID_DEPARTMENT ID_DEPARTMENT,
a.ID_AGENCY ID_AGENCY,
pb.ID_PRODUCT_BILL ID_PRODUCT_BILL,
p.ID_PRODUCT ID_PRODUCT,
pr.ID_PROVIDER ID_PROVIDER,
ds.ID_SALE ID_SALE,
count(1)UNIT,
pb.AMOUNT_OF_BILL PRICE,
t.ID_TIME ID_TIME
FROM TestProSA.dbo.Sales s
inner join TestProDW.dbo.DIM_CLIENT c
on c.ID_CLIENT = s.ClientId
inner join TestProDW.dbo.DIM_EMPLOYEE e
on e.ID_EMPLOYEE = s.EmployeeId
inner join TestProDW.dbo.DIM_DEPARTMENT d
on d.ID_DEPARTMENT = e.DEPARTMENT_CODE
inner join TestProDW.dbo.DIM_AGENCY a
on a.ID_AGENCY = d.AGENCY_CODE
inner join TestProDW.dbo.DIM_PRODUCT_BILL pb
on pb.SALES_CODE = s.SaleId
left join TestProDW.dbo.DIM_PRODUCT p
on p.ID_PRODUCT = pb.PRODUCT_CODE
inner join TestProDW.dbo.DIM_PROVIDER pr
on pr.ID_PROVIDER = p.SUPPLIER_COD
join TestProDW.dbo.DIM_SALE ds
on ds.ID_SALE = s.SaleId
inner join TestProDW.dbo.DIM_TIME t
on cast(t.DATE_TIME as Date)  = cast(s.Date as Date) and (t.ID_TIME = s.SaleId)
group by
a.ID_AGENCY,
c.ID_CLIENT,
d.ID_DEPARTMENT,
e.ID_EMPLOYEE,
p.ID_PRODUCT,
pr.ID_PROVIDER,
t.ID_TIME,
pb.ID_PRODUCT_BILL,
ds.ID_SALE,
pb.AMOUNT_OF_BILL

---------------------------------------------------------------------------------------------------


